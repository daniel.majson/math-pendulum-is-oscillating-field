######################################################################
##              numerical solution of the problem                   ##
##  "Mathematical pendulum in the oscillating gravitational field"  ##
##              using 4th order Runge-Kutta algorithm               ##
######################################################################

import numpy as np
import matplotlib.pyplot as plt
from scipy import optimize

# define constant parameters
g0 = 9.81
freq = 1.
g_magnitude = 0.1
m = 1.
L = 5.

def g(t): 
  return g0 + g_magnitude*np.sin(freq*t)

# define timeframe and step 
time_len = 10.
dt = 0.001
time_arr = np.arange(0., time_len, dt)

# arrays for coordinates and velocity
x = np.empty_like(time_arr)
y = np.empty_like(time_arr)
vel_x = np.empty_like(time_arr)
vel_y = np.empty_like(time_arr)

# initial state
x[0], y[0] = 3., -4.
vel_x[0], vel_y[0] = 0., 0.


# define f(t, Y, T): dY/dt = f(t, Y, T)
# reminder: Y = [x, y, dx/dt, dy/dt]
# T is additional parameter
def f(t, Y, T): 
  return np.array([Y[2], Y[3], -T/(m*L)*Y[0], -T/(m*L)*Y[1] - g(t)])

# define new coordinates and velocity (on this step T is external parameter, not restricted by any condition)
def get_new_coord_and_vel(t, Y, T):
  #classical 4th order Runge-Kutta algorithm
  k1 = f(t, Y, T)
  k2 = f(t + dt / 2., Y + k1 * dt / 2., T)
  k3 = f(t + dt / 2., Y + k2 * dt / 2., T)
  k4 = f(t + dt, Y + k3 * dt, T)
  Y_next = Y + (k1 + 2. * k2 + 2. * k3 + k4) * dt/6.
  return Y_next

# calculate updated coordinates and find the new stick length
def get_new_length(t, Y_cur, T):
  Y_next = get_new_coord_and_vel(t, Y_cur, T)
  return np.sqrt((Y_next[0] * Y_next[0] + Y_next[1] * Y_next[1]))

# define T-force, using the condition of stick length invariability
def get_T_force(t, Y):
  opt_res = optimize.minimize_scalar(lambda T: (get_new_length(t, Y, T)-L)**2.)
  return opt_res.x

# the main part, the iterational procedure
# i is the time_index; starts from 1, since x[0] etc are already defined
for i in range(1, len(time_arr)):
  # print(f'{i = }')
  T = get_T_force(time_arr[i], [x[i-1], y[i-1], vel_x[i-1], vel_y[i-1]]) # find T in this moment
  Y_current = np.array([x[i-1], y[i-1], vel_x[i-1], vel_y[i-1]]) 
  Y_next = get_new_coord_and_vel(time_arr[i-1], Y_current, T) # define new coordinates and velocity
  x[i], y[i], vel_x[i], vel_y[i] = Y_next
print('Calculation is over!')
